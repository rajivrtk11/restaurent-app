class ApiFeature {
  constructor(ActiveRestaurent, queryStr) {
    this.restaurent = ActiveRestaurent;
    this.queryStr = queryStr;
  }
  search() {
    const cityRestaurent = [];

    if (this.queryStr.city) {
      this.queryStr.city=this.queryStr.city.toLowerCase()
      this.restaurent.map((resta) => {
        if (resta.rest.city === this.queryStr.city) {
          cityRestaurent.push(resta);
        }
      });
      this.restaurent = cityRestaurent;
    }

    return this.restaurent;
  }
  filterPrice() {
    let lte;
    let gte;
    
    if (this.queryStr.AverageDiningPrice) {
      gte = Number(
        this.queryStr.AverageDiningPrice.gt
          ? this.queryStr.AverageDiningPrice.gt
          : ""
      );
      lte = Number(
        this.queryStr.AverageDiningPrice.lt
          ? this.queryStr.AverageDiningPrice.lt
          : ""
      );

      const cityRestaurent = [];
      this.restaurent.map((resta) => {
        if (resta.rest.AverageDiningPrice >= gte && resta.rest.AverageDiningPrice <= lte) {
          
          cityRestaurent.push(resta);
        }
      });
      this.restaurent = cityRestaurent;
    }
    return this.restaurent;
  }
}

module.exports = ApiFeature;
