const ErrorHandler = require('../utils/ErrorHandler');



module.exports=(err, req,res,next)=>{
    console.log(err);
    err.statusCode=err.statusCode || 500;
    err.message=err.message || "Interval Server Error"

     //Wrong mongodb id error
     if(err.name==="CastError"){
        console.log(err)
        const message=`Resource not found. Invalid : ${err.path} : ${err.value}`
        err=new ErrorHandler(message,400)
    }

    //Mongoose duplicate Key error
    if(err.code===11000){
        const message=`Duplicate ${Object.keys(err.keyValue)} Entered`
        err=new ErrorHandler(message,400)
    }

     //Wrong JWT Token
     if(err.name==="JsonWebTokenError"){
        const message=`Json web token is invalid , try again`
        err=new ErrorHandler(message,400)
    }

    //JWT Expire Error
    if(err.name==="TokenExpiredError"){
        const message=`Json web token is Expired, try again`
        err=new ErrorHandler(message,400)
    }



    res.status(err.statusCode).json({
        success: false,
        errors:err,
    })
}