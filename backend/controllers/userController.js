const ErrorHandler = require("../utils/errorhandler");
const catchAsyncError = require("../middleware/catchAsyncError");
const User = require("../models/userModel");
const sendToken = require("../utils/jwtToken");
const crypto = require("crypto");
const Restaurent = require("../models/restaurentModel");

//Register a User
exports.registerUser = catchAsyncError(async (req, res, next) => {
  let { name, email, password, role } = req.body;
  const passwordSpaces=password.split(" ").length - 1
  if(passwordSpaces===password.length){
    return next(new ErrorHandler("password is invalid"));
  }
  if (role === "admin") {
    return next(new ErrorHandler("You can not Register as Admin"));
  }
  const user = await User.create({
    name,
    email,
    password,
    role,
    
  });

  if(role==="owner"){
    await Restaurent.create({
      user:user._id,
      numberOfRestaurent: 0,
      restaurent:[]
    })
  }
  
  sendToken(user, 201, res);
});

//Login User
exports.loginUser = catchAsyncError(async (req, res, next) => {
  const { email, password } = req.body;
  //checking if user has given password and email both
  if (!email || !password) {
    return next(new ErrorHandler("Please enter email and password", 400));
  }
  const user = await User.findOne({ email }).select("+password");

  if (!user) {
    return next(new ErrorHandler("Invalid email or password ", 401));
  }
  const isPasswordMatch = await user.comparePassword(password);
  if (!isPasswordMatch) {
    return next(new ErrorHandler("Invalid email or password", 401));
  }
  sendToken(user, 200, res);
});

//Logout User
exports.logout = catchAsyncError(async (req, res, next) => {
  res.cookie("token", null, {
    expires: new Date(Date.now()),
    httpOnly: true,
  });

  res.status(200).json({ success: true, message: "Logged out" });
});

//Get User Details
exports.getUserDetails = catchAsyncError(async (req, res, next) => {
  const user = await User.findById(req.user.id);

  res.status(200).json({
    success: true,
    user,
  });
});

//update password
exports.updatePassword = catchAsyncError(async (req, res, next) => {
  const user = await User.findById(req.user.id).select("+password");
  const isPasswordMatch = await user.comparePassword(req.body.oldPassword);
  if (!isPasswordMatch) {
    return next(new ErrorHandler("Old Password is incorrect", 401));
  }

  if (req.body.newPassword !== req.body.confirmPassword) {
    return next(new ErrorHandler("Password does not match", 401));
  }
  user.password = req.body.newPassword;
  user.save();
  sendToken(user, 200, res);
});

//update userProfile
exports.updateProfile = catchAsyncError(async (req, res, next) => {
  const newUserData = {
    name: req.body.name,
    email: req.body.email,
  };
  // we will add cloudinary later
  const user = await User.findByIdAndUpdate(req.user.id, newUserData, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });
  user.save();
  res.status(200).json({ success: true ,user });
});

//get all users (admin)
exports.getAllUser = catchAsyncError(async (req, res, next) => {
  const users = await User.find();

  res.status(200).json({ success: true, users });
});

//get single user (admin)
exports.getSingleUser = catchAsyncError(async (req, res, next) => {
  const user = await User.findById(req.params.id);
  if (!user) {
    return next(
      new ErrorHandler(`user does not exit with id ${req.params.id}`)
    );
  }
  res.status(200).json({
    success: true,
    user,
  });
});

//Update User role  - Admin
exports.updateUserRole = catchAsyncError(async (req, res, next) => {
  const newUserData = {
    role: req.body.role
  };
  if(newUserData.role==="user"){
    await Restaurent.findOneAndUpdate({user : req.params.id},{previousOwner:req.params.id,user:req.user.id})
  }
  const user = await User.findByIdAndUpdate(req.params.id, newUserData, {
    new: true,
    runValidators: true,
    useFindAndModify: false,
  });
  if(newUserData.role==="owner"){
    await Restaurent.findOneAndUpdate({previousOwner:req.params.id},{previousOwner:null,user:req.params.id})
  }
  user.save();
  
  res.status(200).json({ success: true });
});

//Delete User --Admin
exports.deleteUser = catchAsyncError(async (req, res, next) => {
  const user = await User.findById(req.params.id);    
  let adminRestaurent=new Array();
  if (!user) {
    return next(
      new ErrorHandler(`User does not exist with id ${req.params.id}`)
    );
  }
  const restaurents=await Restaurent.find({user:req.params.id});
  const admin=await Restaurent.find({user:req.user.id})
  admin[0].restaurent.map(resta=>{
    adminRestaurent.push(resta)
  })
  restaurents[0]?.restaurent.map(resta=>{
    adminRestaurent.push(resta);
  })  
  admin[0].numberOfRestaurent=adminRestaurent.length;
  admin[0].restaurent=adminRestaurent;
  admin[0].save();
  await Restaurent.remove({user:req.params.id})
  await User.remove({_id:req.params.id});
  res
    .status(200)
    .json({ success: true, message: "User deleted successfully " });
});
