const catchAsyncError = require("../middleware/catchAsyncError");
const Restaurent = require("../models/restaurentModel");
const User = require("../models/userModel");
const ErrorHandler = require("../utils/errorhandler");
const mongoose = require("mongoose");
const ApiFeature = require("../utils/ApiFeature");

exports.createRestaurents = catchAsyncError(async (req, res, next) => {
  const { restaurent } = req.body;
  restaurent.city = restaurent.city.toLowerCase();

  const restaurentRow = await Restaurent.findOne({
    user: req.user.id,
    previousOwner: null,
  });

  if (restaurentRow) {
    restaurentRow.restaurent.push(restaurent);
    restaurentRow.numberOfRestaurent = restaurentRow.restaurent.length;
    restaurentRow.save();
  } else {
    await Restaurent.create({
      user: req.user.id,
      numberOfRestaurent: 1,
      restaurent,
    });
  }
  res.status(200).json({
    success: true,
    message: "restaurent Created",
    Restaurents: restaurentRow,
  });
});

//get all restaurents  --- Admin
exports.getAllRestaurents = catchAsyncError(async (req, res, next) => {
  const restaurents = await Restaurent.find();
  res.status(200).json({
    success: true,
    restaurents,
  });
});

//get your own restaurent
exports.getOwnRestaurents = catchAsyncError(async (req, res, next) => {
  const restaurents = await Restaurent.find({ user: req.user.id });
  res.status(200).json({
    success: true,
    YourRestaurents: restaurents,
  });
});

//get all active restaurent - user admin  owner
exports.getAllActiveRestaurents = catchAsyncError(async (req, res, next) => {
  const restaurents = await Restaurent.find();
  const allRestaturent = [];
  restaurents.map((res) => {
    res.restaurent.map((rest) => {
      allRestaturent.push({ owner: res.user, rest });
    });
  });
  const ActiveRestaurents = allRestaturent.filter(
    (res) => res.rest.status === "Active"
  );

  const filterCityRestaurent = new ApiFeature(
    ActiveRestaurents,
    req.query
  ).search();
  const filterPriceRestaurent = new ApiFeature(
    filterCityRestaurent,
    req.query
  ).filterPrice();

  res.status(200).json({
    success: true,
    restaurents: filterPriceRestaurent,
  });
});

//delete own Restaurent
exports.deleteRestaurent = catchAsyncError(async (req, res, next) => {
  const restaurentId = req.params.rid;

  const restaurents = await Restaurent.find({
    user: req.user.id,
  });

  if (restaurents[0].restaurent.length === 0) {
    return next(new ErrorHandler("you don not own any restaurent"));
  }

  let findResta;
  restaurents.map((resta) => {
    findResta = resta.restaurent.find((rest) => rest.id === restaurentId);
    if (findResta) {
      resta.restaurent.remove(findResta);
      resta.numberOfRestaurent = resta.restaurent.length;
      resta.save();
    }
  });

  if (!findResta) {
    return next(
      new ErrorHandler("you don not own any restaurent with this id", 404)
    );
  }

  res.status(200).json({ success: true, restaurents });
});

//delete restaurent -- Admin
exports.deleteOwnerRestaurent = catchAsyncError(async (req, res, next) => {
  const userId = req.params.uid;
  const restId = req.params.rid;

  const ownerRestaurent = await Restaurent.find({ user: userId });
  if (!ownerRestaurent) {
    return next(new ErrorHandler("Owner with this id is not present"));
  }

  const rest = ownerRestaurent[0].restaurent.find((rest) => rest.id === restId);
  if (!rest) {
    return next(new ErrorHandler("Restaurent with this id is not present"));
  }

  ownerRestaurent[0].numberOfRestaurent -= 1;

  ownerRestaurent[0].restaurent.remove(rest);
  ownerRestaurent[0].save();

  res.status(200).json({ success: true, ownerRestaurent });
});

//edit restaurent By Owner
exports.editRestaurent = catchAsyncError(async (req, res, next) => {
  const restaurentId = req.params.rid;
  const { restaurentName, AverageDiningPrice, city, status } = req.body;

  const ownerRestaurent = await Restaurent.find({ user: req.user.id });

  if (ownerRestaurent[0].restaurent.length === 0) {
    return next(new ErrorHandler("you don not own any restaurent"));
  }
  let findResta;
  ownerRestaurent.map((resta) => {
    findResta = resta.restaurent.find((rest) => rest.id === restaurentId);
    if (findResta) {
      findResta.restaurentName = restaurentName;
      findResta.AverageDiningPrice = AverageDiningPrice;
      findResta.city = city;
      findResta.status = status;
      resta.save();
    }
  });
  if (!findResta) {
    return next(
      new ErrorHandler("Restaurent with this id is not present", 404)
    );
  }
  res.status(200).json({ success: true, message: "Restaurent Updated" });
});

exports.editOwnerRestaurent = catchAsyncError(async (req, res, next) => {
  const userId = req.params.uid;
  const restId = req.params.rid;
  const { restaurentName, AverageDiningPrice, city, status } = req.body;

  const ownerRestaurent = await Restaurent.find({ user: userId });
  if (!ownerRestaurent) {
    return next(new ErrorHandler("Owner with this id is not present"));
  }

  const rest = ownerRestaurent[0].restaurent.find((rest) => rest.id === restId);
  if (!rest) {
    return next(new ErrorHandler("Restaurent with this id is not present"));
  }
  rest.restaurentName = restaurentName;
  rest.AverageDiningPrice = AverageDiningPrice;
  rest.city = city;
  rest.status = status;
  ownerRestaurent[0].save();

  res
    .status(200)
    .json({ success: true, message: "Restaurent Updated By Admin" });
});
