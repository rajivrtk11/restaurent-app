const express=require("express");
const { createRestaurents, getAllRestaurents,getOwnRestaurents,getAllActiveRestaurents,editOwnerRestaurent,editRestaurent,deleteRestaurent,deleteOwnerRestaurent} = require("../controllers/restaurentController");
const { isAuthenticatedUser,authorizeRoles,authorizeAdminManager} = require("../middleware/auth");
const router=express.Router();

//get All Active Restaurent - User or all 
router.route("/allrestaurents").get(getAllActiveRestaurents)

//create Restaurent only by owner and admin
router.route("/restaurent/new").post(isAuthenticatedUser,authorizeAdminManager(), createRestaurents);


//get restaurent own by you
router.route("/restaurents/me").get(isAuthenticatedUser,authorizeAdminManager(),getOwnRestaurents);
//delete restaurent own by you 
router.route("/restaurent/:rid").delete(isAuthenticatedUser,authorizeAdminManager(),deleteRestaurent);
//edit Restaurent own by you 
router.route("/restaurent/:rid").patch(isAuthenticatedUser,authorizeAdminManager(),editRestaurent);



//get all restaurent --Admin
router.route("/restaurents").get(isAuthenticatedUser,authorizeRoles("admin"),getAllRestaurents);

////delete Restaurent --- Admin
router.route("/restaurent/:uid/:rid").delete(isAuthenticatedUser,authorizeAdminManager(),deleteOwnerRestaurent);

//edit owner restaurent -- Admin
router.route("/restaurent/:uid/:rid").patch(isAuthenticatedUser,authorizeRoles("admin"),editOwnerRestaurent);



module.exports=router;