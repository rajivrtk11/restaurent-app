const mongoose = require("mongoose");

const restaurentSchema = new mongoose.Schema({
  user: {
    type: mongoose.Schema.ObjectId,
    ref: "User",
    required: true,
  },
  numberOfRestaurent: {
    type: Number,
    default: 0,
  },
  restaurent: [
    {
      restaurentName: {
        type: String,
        required: true,
      },
      city: {
        type: String,
        require: true,
      },
      AverageDiningPrice: {
        type: Number,
        required: true,
      },
      status: {
        type: String,
        required: true,
      },
    },
  ],
  previousOwner:{
    type: mongoose.Schema.ObjectId,
    ref: "User",
    default:null,
  },

});

module.exports = mongoose.model("Restaurent", restaurentSchema);
