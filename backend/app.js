
const express=require("express");
const app=express();    
const cookieParser=require("cookie-parser");
const errorMiddleware = require('./middleware/error');

app.use(express.json())
app.use(cookieParser())

const user=require("./routes/userRoutes");
const restaurent=require("./routes/restaurentRoutes")

app.use("/api/v1",user)
app.use("/api/v1",restaurent);


app.use(errorMiddleware);

module.exports=app;